@extends('template')

@section('title', 'Gallery')


@section('content')
<h4 class="text-center">Gallery</h4>
<div class="row">
    <div class="col-md-4">
        <img src="{{asset('image/keluarga1.jpg')}}" class="rounded float-start w-100" alt="...">
    </div>
    <div class="col-md-4">
        <img src="{{asset('image/keluarga2.jpg')}}" class="rounded float-start w-100" alt="...">
    </div>
    <div class="col-md-4">
        <img src="{{asset('image/keluarga3.jpg')}}" class="rounded float-start w-100" alt="...">
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <img src="{{asset('image/keluarga4.jpg')}}" class="rounded float-start w-100" alt="...">
    </div>
    <div class="col-md-4">
        <img src="{{asset('image/crawel.jpg')}}" class="rounded float-start w-100" alt="...">
    </div>
    <div class="col-md-4">
        <img src="{{asset('image/workshop.jpg')}}" class="rounded float-start w-100" alt="...">
    </div>
</div>
@endsection
