
@extends('template')

@section('title', 'Portofolio')


@section('content')
<div class="row featurette">
    <div class="col-md-7">
      <h2 class="featurette-heading fw-normal lh-1">Sistem Manjemen Pelatihan.</span></h2>
      <p class="lead">Kementrian Sekretariat Negara membangun Sistem Manajemen pelatihan untuk mengatur segala aktifitas yang dilakukan dalam sebuah pelatihan, dari membuat tanggal acara, instruktur sampai terbuatnya sertifikat otomatis yang langsung terkirim ke peserta kegiatan.</p>
    </div>
    <div class="col-md-5">
        <img src="{{ asset('image/pionir.png') }}" class="border img-fluid w-100"  width="500" height="500">

    </div>
  </div>

  <hr class="featurette-divider">

  <div class="row featurette">
    <div class="col-md-7 order-md-2">
      <h2 class="featurette-heading fw-normal lh-1">Sistem Akademik</h2>
      <p class="lead">Kampus STISIP Widuri membangun Sistem akademik untuk mengatur segala aktifitas perkuliahan dari pendaftaran, pembayaran semester mahasiswa, sampai terbuatnya transkip mahasiswa .</p>
    </div>
    <div class="col-md-5 order-md-1">
        <img src="{{ asset('image/siakad.png') }}" class="border img-fluid w-100"  width="500" height="500">
    </div>
  </div>


  <hr class="featurette-divider">

  <div class="row featurette">
    <div class="col-md-7">
      <h2 class="featurette-heading fw-normal lh-1">Sistem Penunjang Keputusan.</span></h2>
      <p class="lead">Saat ini banyak kebutuhan dalam mengambil keputusan, oleh karna itu kami membangun sistem penunjang keputusan untuk menentukan yang layak mendapatkan beasiswa dengan metode AHP dan Topsis .</p>
    </div>
    <div class="col-md-5">
        <img src="{{ asset('image/spk.png') }}" class="border img-fluid w-100"  width="500" height="500">
    </div>
  </div>


@endsection
