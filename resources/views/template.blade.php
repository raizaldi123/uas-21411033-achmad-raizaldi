<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.101.0">
    <title>@yield('title')</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/navbar-fixed/">

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <meta name="theme-color" content="#712cf9">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        h1 {
            font-size: 10vw;
        }

        @media only screen and (max-width: 500px) {
            .card{
                padding: 0px;
            }
            .card-body {
                margin-top: 100px;
                font-size: 10px;
            }
            h1{
                font-size: 12px;
            }
            p{
                margin:0px;
                padding:0px;
            }
        }


        /* Large devices (laptops/desktops, 992px and up) -- This would display the text at 10.0rem on laptops and larger screens */

        @media only screen and (min-width: 992px) {
            h1 {
                font-size: 1.5rem;
            }
        }

        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .b-example-divider {
            height: 3rem;
            background-color: rgba(0, 0, 0, .1);
            border: solid rgba(0, 0, 0, .15);
            border-width: 1px 0;
            box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
        }

        .b-example-vr {
            flex-shrink: 0;
            width: 1.5rem;
            height: 100vh;
        }

        .bi {
            vertical-align: -.125em;
            fill: currentColor;
        }

        .nav-scroller {
            position: relative;
            z-index: 2;
            height: 2.75rem;
            overflow-y: hidden;
        }

        .nav-scroller .nav {
            display: flex;
            flex-wrap: nowrap;
            padding-bottom: 1rem;
            margin-top: -1px;
            overflow-x: auto;
            text-align: center;
            white-space: nowrap;
            -webkit-overflow-scrolling: touch;
        }

        .card {
            margin-right: auto;
            margin-left: auto;
            box-shadow: 0 15px 25px rgba(129, 124, 124, 0.2);
            border-radius: 5px;
            backdrop-filter: blur(14px);
            background-color: rgba(255, 255, 255, 0.2);
            padding: 10px;
            text-align: center;
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="{{ asset('css/navbar-top-fixed.css') }}" rel="stylesheet">
</head>

<body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Achmad Raizaldi</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <?php
                $path = Route::current()->getName();
                $current = str_replace('.index', '', $path);
                ?>
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                        <a class="nav-link <?php echo $current == '' || $current == 'home' ? 'active' : ''; ?>" aria-current="page" href="home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo $current == 'about' ? 'active' : ''; ?>" href="about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo $current == 'portofolio' ? 'active' : ''; ?> " href="portofolio">Portofolio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo $current == 'contact' ? 'active' : ''; ?> " href="contact">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo $current == 'gallery' ? 'active' : ''; ?> " href="gallery">Gallery</a>
                    </li>

                </ul>

            </div>
        </div>
    </nav>

    <main class="container">
        @yield('content')
    </main>


    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>


</body>

</html>
