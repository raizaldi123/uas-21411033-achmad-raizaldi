@extends('template')

@section('title', 'About')


@section('content')
    <div class="container card mt-3">
        <h1 class="text-center ">Full Stack Developer</h1>
        <div class="card-body m-0">
            <div class="font-weight-bold">
                Saat ini bekerja sebagai developer coordinator sejak tahun 2016 di perusahaan PT.Generasi Inspirasi Informatika <a
                    href="https://dealpos.co.id">(DealPOS)</a> yakni
                perusahaan yang memperlengkapi bisnis dengan software yang membantu usaha kecil dan menengah dalam
                mengelolah
                usahanya.
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li class="d-flex justify-content-start"><i class="bi bi-chevron-right"></i>
                                <strong>Birthday:</strong> <span>11 Agustus 1992</span></li>
                            <li class="d-flex justify-content-start"><i class="bi bi-chevron-right"></i>
                                <strong>Website:</strong> <span>www.tukangcode.com</span></li>
                            <li class="d-flex justify-content-start"><i class="bi bi-chevron-right"></i>
                                <strong>Phone:</strong> <span>+628 1574945443</span></li>
                            <li class="d-flex justify-content-start"><i class="bi bi-chevron-right"></i>
                                <strong>City:</strong> <span>Jakarta, Indonesia</span></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <li class="d-flex justify-content-start"><i class="bi bi-chevron-right"></i>
                                <strong>Age:</strong> <span>30</span></li>
                            <li class="d-flex justify-content-start"><i class="bi bi-chevron-right"></i>
                                <strong>Degree:</strong> <span>D3</span></li>
                            <li class="d-flex justify-content-start"><i class="bi bi-chevron-right"></i>
                                <strong>Email:</strong> <span>achmad.raizaldi@gmail.com</span></li>
                            <li class="d-flex justify-content-start"><i class="bi bi-chevron-right"></i>
                                <strong>Freelance:</strong> <span>Available</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
