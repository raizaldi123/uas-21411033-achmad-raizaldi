@extends('template')

@section('title', 'Contact')


@section('content')
    <h3 class="text-center">Contact</h3>
    <form action="" method="post">
        <div class="row">
            <div class="col-md-6">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="floatingInput" placeholder="Name">
                    <label for="floatingInput"> Name</label>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-floating mb-3">
                    <input type="email" class="form-control" id="floatingInput" placeholder="Email">
                    <label for="floatingInput"> Email</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="floatingInput" placeholder="Subject">
                    <label for="floatingInput"> Subject</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-floating">
                <textarea class="form-control" placeholder=" your message" id="floatingTextarea"></textarea>
                <label for="floatingTextarea">&nbsp;Your message</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt-2 gap">
                <button type="button" class="btn btn-outline-primary">Send your message</button>
            </div>
        </div>
    </form>

@endsection
