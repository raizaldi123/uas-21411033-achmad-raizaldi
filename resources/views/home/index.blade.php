@extends('template')

@section('title', 'Home')


@section('content')

    <div id="myCarousel" class="carousel slide mt-3 border" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" class="active" aria-current="true"
                aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('image/pionir.png') }}" class="rounded img-fluid w-100" alt="">

                <div class="container">
                    <div class="carousel-caption text-start text-dark card">
                        <div class="card-body">
                            <h1>Sistem Manajemen Pelatihan.</h1>
                            <p>Sistem Manajemen pelatihan mengatur segala aktifitas yang dilakukan dalam sebuah pelatihan,
                                dari membuat tanggal acara, instruktur sampai terbuatnya sertifikat otomatis yang langsung
                                terkirim ke peserta kegiatan.</p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{ asset('image/siakad.png') }}" class="rounded img-fluid w-100" alt="">

                <div class="container">
                    <div class="carousel-caption text-dark card">
                        <div class="card-body">
                            <h1>Sistem Akademik.</h1>
                            <p>Sistem akademik mengatur segala aktifitas perkuliahan dari pendaftaran, pembayaran semester
                                mahasiswa, sampai terbuatnya transkip mahasiswa .</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>


    <div class="container mt-2">

        <!-- Three columns of text below the carousel -->
        <div class="row">
            <div class="col-md-3 text-center">
                <i class="fa fa-facebook-square" style="font-size: 7.333333em;color: #3636c9;" aria-hidden="true"></i>

                <h2 class="fw-normal">Facebook</h2>
                <p><a class="btn btn-small btn-primary" href="https://www.facebook.com/anaknyabangali">View details
                        &raquo;</a></p>
            </div><!-- /.col-lg-4 -->
            <div class="col-md-3 text-center">
                <i class="fa fa-instagram" style="font-size: 7.333333em;color: #a663ff;" aria-hidden="true"></i>

                <h2 class="fw-normal">Instagram</h2>
                <p><a class="btn  btn-small btn-primary" href="https://www.instagram.com/achmadraizaldi/">View details
                        &raquo;</a></p>
            </div><!-- /.col-lg-4 -->
            <div class="col-md-3 text-center">
                <i class="fa fa-linkedin-square" style="font-size: 7.333333em;color: #3636c9;" aria-hidden="true"></i>

                <h2 class="fw-normal">LinkedIn</h2>
                <p><a class="btn  btn-small btn-primary" href="https://www.linkedin.com/in/achmad-raizaldi/">View details
                        &raquo;</a></p>
            </div><!-- /.col-lg-4 -->
            <div class="col-md-3 text-center">
                <i class="fa fa-whatsapp" style="font-size: 7.333333em;color: #24b91a;" aria-hidden="true"></i>

                <h2 class="fw-normal">Whatshapp</h2>
                <p><a class="btn  btn-small btn-primary" href="https://api.whatsapp.com/send/?phone=6281574945443&text=Hi+raizaldi+saya+mau+tanya&type=phone_number&app_absent=0">View details
                        &raquo;</a></p>
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->


    </div><!-- /.container -->

@endsection
